package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Article implements Serializable{

	@Id
	@GeneratedValue
	//	@Column(name="id_article")
	private Long idArticle;

	private String codeArticle;
	private String designation;
	private BigDecimal prixUnitaireHT;
	private BigDecimal tauxTva;
	private BigDecimal prixunitaireTTC;
	private String photo;
	
	@ManyToOne
	@JoinColumn(name="idCategoty")
	private Category category;
	
//	@OneToMany(mappedBy="article")
//	private List<LigneCommandeClient> ligneCommandeClients;
//	@OneToMany(mappedBy="article")
//	private List<LigneCommandeFournisseur> ligneCommandeFournisseurs;
//	@OneToMany(mappedBy="article")
//	private List<LigneVente> ligneVentes;
//	@OneToMany(mappedBy="article")
//	private List<MvtStck> mvtStcks;

	public Article() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}

	public String getCodeArticle() {
		return codeArticle;
	}

	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixUnitaireHT() {
		return prixUnitaireHT;
	}

	public void setPrixUnitaireHT(BigDecimal prixUnitaireHT) {
		this.prixUnitaireHT = prixUnitaireHT;
	}

	public BigDecimal getTauxTva() {
		return tauxTva;
	}

	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}

	public BigDecimal getPrixunitaireTTC() {
		return prixunitaireTTC;
	}

	public void setPrixunitaireTTC(BigDecimal prixunitaireTTC) {
		this.prixunitaireTTC = prixunitaireTTC;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}


}
